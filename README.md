# fluentd-test

## Steps
- docker build -t jli/fluentd:v1.12-debian .
- kubectl create deployment --image=jli/fluentd:v1.12-debian fluentd-demo
- kubectl port-forward pod/fluentd-demo-794db4f59d-zsxqx  9880:9880
- kubectl exec kafka-client  -- kafka-console-consumer --bootstrap-server kafka:9092 --topic messages  --from-beginning
- curl -X POST -d 'json={"json":"message", "country":"Canada"}' http://localhost:9880/kafka.test


## already made images
docker pull fluent/fluentd-kubernetes-daemonset:v1.12-debian-kafka2-1